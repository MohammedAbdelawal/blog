from django.contrib import admin
from .models import Profile, Category, Article, Comment

# Register your models here.

admin.site.register(Profile)
admin.site.register(Category)
admin.site.register(Article)
admin.site.register(Comment)



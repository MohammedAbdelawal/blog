from django.shortcuts import render
from .forms import UserForm, ProfileForm
from django.contrib.auth import authenticate, login , logout
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from . import models
from django.views.generic import ListView
# Create your views here.

def home(request):
    return render(request,'baseBlog.html')

def register(request):
    if request.method == 'POST':
        userForm = UserForm(request.POST )
        profileForm = ProfileForm(request.POST ,request.FILES )
        
        if userForm.is_valid() and profileForm.is_valid():
            user = userForm.save()
            user.set_password(user.password)
            user.save()
            profile = profileForm.save(commit=False)
            profile.user = user
            if 'pic' in request.FILES:
                profile.pic = request.FILES['pic']
            profile.save() 
            login(request,user)
            return HttpResponseRedirect(reverse('home'))


        else:
            print(userForm.errors)
            print(profileForm.errors)
    else:
        userForm = UserForm()
        profileForm = ProfileForm()

    return render(request, 'register.html', {
        'userForm':userForm,
        'profileForm':profileForm    })
    
def loginPage(request):
    if request.method == 'POST':
        userName =request.POST.get('username')
        password =request.POST.get('password')
        user = authenticate(username=userName,password=password)
        if user:
            login(request,user)
            return HttpResponseRedirect(reverse('home'))
        else:
            return HttpResponse('invalid user')
    else:
        return render(request,'login.html')

@login_required
def logoutPage(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))

class CategoryListView(ListView):
    model = models.Category
    template_name = 'category_list.html'
        